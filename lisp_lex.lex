%option noyywrap

%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TOKEN_ERROR -1
#define TOKEN_UNKNOWN -2
#define TOKEN_IDENTIFIER 1
#define TOKEN_NUMBER 2
#define TOKEN_LBRACKET 3
#define TOKEN_RBRACKET 4
#define TOKEN_WHITESPACE 5
#define TOKEN_ENDLINE 6

#define MAX_TOKEN_LEN 128
#define MAX_ERR_LEN 1024

int  currentToken;
char currentTokenStr[MAX_TOKEN_LEN];
double  currentTokenDouble = 0;
char currentTokenError[MAX_ERR_LEN];

%}

%%

\(				{
  currentToken = TOKEN_LBRACKET;
  return currentToken;
}

\)				{
  currentToken = TOKEN_RBRACKET;
  return currentToken;
}

[+-]?[0-9]+\.?[0-9]*		{
  currentToken = TOKEN_NUMBER;
  currentTokenDouble = strtod(yytext, NULL);
  return currentToken;
}

[0-9a-zA-Z_\+\-\*\/\=\<\>\!]*		{
  currentToken = TOKEN_IDENTIFIER;
  if (yyleng < MAX_TOKEN_LEN)
    strcpy(currentTokenStr, yytext);
  else
  {
    currentToken = TOKEN_ERROR;
    strcpy(currentTokenError, "Indentifier too long.");
  }

  return currentToken;
}

[ \t]*				{
  currentToken = TOKEN_WHITESPACE;
  return currentToken;
}

\n				{
  currentToken = TOKEN_ENDLINE;
  return currentToken;
}

.				{
  currentToken = TOKEN_ERROR;
  strcpy(currentTokenError, "Forbidden symbol used.");
  return currentToken;
}

%%

