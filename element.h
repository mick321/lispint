#ifndef _ELEMENT_H
#define _ELEMENT_H

#include <vector>
#include <string>

/*
  Datovy typ elementu.
*/
enum ElementType
{
  etUnknown = 0,
  etNumber,
  etIdentifier,
  etList,
  etError,
  etNil,
  etTrue
};

typedef double lisp_number;
#define LISP_NUMBER_PRINT_FORMAT "%lg"

/*
  Element = atom/list.
*/
struct Element
{
  Element* parent;
  ElementType type;

  std::vector<Element> children;
  std::string stringValue;
  lisp_number numberValue;

  inline Element()
  {
    type = etUnknown;
    this->parent = NULL;
  }

  inline Element(Element* parent)
  {
    type = etUnknown;
    this->parent = parent;
  }

  void Print(bool linebreak = true);
};

/*
  Sestavuje postupne celou strukturu elementu (listy a atomy).
*/
class ElementBuilder
{
private:
  Element* e;
  Element* level;
public:
  /*
    Konstruktor, vyzaduje jako parametr element, ktery budeme stavet.
  */
  ElementBuilder(Element* e)
  {
    this->e = e;
    Reset();
  }

  /*
    Zacni budovat odznovu.
  */
  void Reset();
  /*
    Pridej cislo do aktualni urovne.
  */
  void AddNumber(const lisp_number num);
  /*
    Pridej identifikator.
  */
  void AddIdentifier(const std::string& identifier);
  /*
    Pridej list.
  */
  void AddList();
  /*
    Zakonci rozpracovany list.
  */
  void EndList();

  Element& GetBegin() { return e->children[0]; }
};


#endif
