#ifndef _EVAL_H
#define _EVAL_H

#include <stdio.h>
#include <map>
#include <string>
#include <vector>

#include "element.h"

enum cmptype
{
  cmpless,
  cmplequal, 
  cmpequal,
  cmpgequal,
  cmpgreater,
  cmpnequal
};

/*
  Trida pro vyhodnoceni elementu.
*/
class ElementEvaluation
{
private:
  Element add(Element& e);
  Element sub(Element& e);
  Element mul(Element& e);
  Element div(Element& e);
  Element id(Element& e);
  Element quote(Element& e);
  Element car(Element& e);
  Element cdr(Element& e);
  Element cmp(Element& e, cmptype op);
  Element cond(Element& e);

  Element defun(Element& e);
  Element callcustom(Element& e);
  Element setid(Element& e);

  Element list(Element& e);

  std::map<std::string, Element> identifiers;
  std::vector<std::map<std::string, Element> > localVariables;
  std::map<std::string, Element> functions;
public:
  /*
    Konstruktor.
  */
  ElementEvaluation();

  /*
    Vyhodnoceni vyrazu.
  */
  Element Evaluate(Element& e);
};

#endif
