#include <stdio.h>
#include "lispint.h"
#include "eval.h"

// --- promenne ---

Element rootElement(NULL);
ElementBuilder rootBuilder(&rootElement);
ElementEvaluation rootEval;

// --- funkce (deklarace) ---

int expression(bool lexread = true);
int sequence();


// --- funkce ----

void welcome()
{
  printf("\n-----------------------------------------\n");
  printf("Basic LISP interpreter, Michal Zak, 2013.\n");
  printf("Welcome!\n");
  printf("-----------------------------------------\n\n");
}

void goodbye()
{
  printf("\nGoodbye!\n\n");
}

void init()
{
}

/*
  Uzitecne makro pro osetreni lexikalnich chyb.
*/
#define TOKEN_ERROR_UNKNOWN \
  case TOKEN_ERROR: \
    fprintf(stderr, "Lexical error: %s\n", currentTokenError); \
    return -1; \
  case TOKEN_UNKNOWN: \
    fprintf(stderr, "Lexical error: Unknown token '%s'\n", yytext); \
    return -1; \
  default: \
    fprintf(stderr, "Syntax error: Unexpected token '%s'\n", yytext); \
    return -1;

int sequence()
{
  int rtn = 0;
seq_again:
  if (yylex() == 0)
    return -2;
  switch (currentToken)
  {
    case TOKEN_RBRACKET: 
      rootBuilder.EndList();
      break;
    case TOKEN_ENDLINE:
    case TOKEN_WHITESPACE: goto seq_again;
    case TOKEN_LBRACKET:
    case TOKEN_IDENTIFIER:
    case TOKEN_NUMBER:
      rtn = expression(false);
      if (rtn != 0)
	return rtn;
      goto seq_again;
    TOKEN_ERROR_UNKNOWN
  }
  return 0;
}

/*
  Funkce pravidla
*/

/*
  Funkce pravidla gramatiky expr -> ID | NUM | LIST
*/
int expression(bool lexread)
{
expr_again:

  if (lexread)
    if (yylex() == 0)
      return -2;
  lexread = true;

  switch (currentToken)
  {
    case TOKEN_NUMBER: 
      //printf("NUMBER %lf\n", currentTokenDouble);
      rootBuilder.AddNumber((lisp_number)currentTokenDouble);
      break;
    case TOKEN_IDENTIFIER: 
      //printf("IDENTIFIER %s\n", currentTokenStr); 
      rootBuilder.AddIdentifier(currentTokenStr);
      break;
    case TOKEN_LBRACKET: 
      rootBuilder.AddList();
      return sequence();
    case TOKEN_ENDLINE: 
      return -3;
    case TOKEN_WHITESPACE: goto expr_again;
    TOKEN_ERROR_UNKNOWN
  }
  return 0;
}

/*
  Beh interpretu LISPU. Opakovane vyhodnocuje vyrazy.
*/
int run()
{
  bool finished = false;
  printf("lisp> ");
  bool doNotEval = false;

  while (!finished)
  {
    if (currentToken == TOKEN_ENDLINE)
    {
      doNotEval = false;
      printf("lisp> ");
    }
    fflush(stdout);

    rootBuilder.Reset();
    int expr_rtn = expression();

    switch (expr_rtn)
    {
      case 0: if (!doNotEval) rootEval.Evaluate(rootBuilder.GetBegin()).Print(); break;
      case -1: doNotEval = true; break;
      case -2: finished = true; break;
      default:;
    }
  }

  return 0;
}

/*
  Funkce main, vstupni bod programu.
*/
int main(int argc, char* argv[])
{
  welcome();
  init();
  int rtn = run();
  if (rtn == 0)
    goodbye();
  return rtn;
}
