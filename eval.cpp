#include "eval.h"
#include <string.h>

/*
  Konstruktor.
*/
ElementEvaluation::ElementEvaluation()
{
}

/*
  Vyhodnoceni vyrazu.
*/
Element ElementEvaluation::Evaluate(Element& e)
{
  switch (e.type)
  {
  case etIdentifier:
    return id(e);
  case etList:
    return list(e);
  default:
    return e;
  }
}

Element ElementEvaluation::list(Element& e)
{
  Element rtn = e;
  if (e.children.size() == 0)
  {
    rtn.type = etNil;
  }
  else
  {
    std::string fname = e.children[0].stringValue;
    if (fname == "+")
      rtn = add(e);
    else if (fname == "-")
      rtn = sub(e);
    else if (fname == "*")
      rtn = mul(e);
    else if (fname == "/")
      rtn = div(e);
    else if (fname == "quote")
      rtn = quote(e);
    else if (fname == "car")
      rtn = car(e);
    else if (fname == "cdr")
      rtn = cdr(e);
    else if (fname == "<")
      rtn = cmp(e, cmpless);
    else if (fname == "<=")
      rtn = cmp(e, cmplequal);
    else if (fname == "=")
      rtn = cmp(e, cmpequal);
    else if (fname == ">=")
      rtn = cmp(e, cmpgequal);
    else if (fname == ">")
      rtn = cmp(e, cmpgreater);
    else if (fname == "/=" || fname == "!=")
      rtn = cmp(e, cmpnequal);
    else if (fname == "if")
      rtn = cond(e);
    else if (fname == "defun")
      rtn = defun(e);
    else if (fname == "setq")
      rtn = setid(e);
    else if (functions.count(fname))
      rtn = callcustom(e);
    else
    {
      if (fname.empty() && e.children[0].type == etNumber)
      {
	char temp[256];
	sprintf(temp, LISP_NUMBER_PRINT_FORMAT, e.children[0].numberValue);
	fname = temp;
      }
      rtn.type = etError;
      rtn.stringValue = "'" + fname + "' is not a function name.";
    }
  }

  return rtn;
}

Element ElementEvaluation::add(Element& e)
{
  Element rtn(e.parent);
  rtn.type = etNumber;
  rtn.numberValue = 0;

  for (int i = 1; i < (int)e.children.size(); i++)
  {
    Element child = Evaluate(e.children[i]);
    if (child.type == etError)
      return child;

    if (child.type != etNumber)
    {
      rtn.type = etError;
      rtn.stringValue = "Addition ('+') expects numbers.";
      return rtn;
    }
    rtn.numberValue += child.numberValue;
    //printf("%lf\n", rtn.numberValue);
  }

  return rtn;
}

Element ElementEvaluation::sub(Element& e)
{
  Element rtn(e.parent);
  if (e.children.size() == 1)
  {
    rtn.type = etError;
    rtn.stringValue = "Missing arguments for subtraction ('-')";
  }
  else if (e.children.size() >= 2)
  {
    rtn = Evaluate(e.children[1]);
    if (rtn.type == etError)
      return rtn;
    if (rtn.type != etNumber)
    {
      rtn.type = etError;
      rtn.stringValue = "Subtraction ('-') expects numbers.";
      return rtn;
    }

    if (e.children.size() == 2)
      rtn.numberValue = -rtn.numberValue;

    for (int i = 2; i < (int)e.children.size(); i++)
    {
      Element child = Evaluate(e.children[i]);
      if (child.type == etError)
        return child;
      if (child.type != etNumber)
      {
        rtn.type = etError;
        rtn.stringValue = "Subtraction ('-') expects numbers.";
        return rtn;
      }
      rtn.numberValue -= child.numberValue;
      //printf("%lf\n", rtn.numberValue);
    }

  }

  return rtn;
}

Element ElementEvaluation::mul(Element& e)
{
  Element rtn(e.parent);
  rtn.type = etNumber;
  rtn.numberValue = 1;

  for (int i = 1; i < (int)e.children.size(); i++)
  {
    Element child = Evaluate(e.children[i]);
    if (child.type == etError)
      return child;

    if (child.type != etNumber)
    {
      rtn.type = etError;
      rtn.stringValue = "Multiplication ('*') expects numbers.";
      return rtn;
    }
    rtn.numberValue *= child.numberValue;
    //printf("%lf\n", rtn.numberValue);
  }

  return rtn;
}

Element ElementEvaluation::div(Element& e)
{
  Element rtn(e.parent);
  if (e.children.size() == 1)
  {
    rtn.type = etError;
    rtn.stringValue = "Missing arguments for division ('/')";
  }
  else if (e.children.size() >= 2)
  {
    rtn = Evaluate(e.children[1]);
    if (rtn.type == etError)
      return rtn;
    if (rtn.type != etNumber)
    {
      rtn.type = etError;
      rtn.stringValue = "Division ('/') expects numbers.";
      return rtn;
    }

    if (e.children.size() == 2)
      rtn.numberValue = 1 / rtn.numberValue;
    else
      rtn.numberValue = rtn.numberValue;

    for (int i = 2; i < (int)e.children.size(); i++)
    {
      Element child = Evaluate(e.children[i]);
      if (child.type == etError)
        return child;
      if (child.type != etNumber)
      {
        rtn.type = etError;
        rtn.stringValue = "Division ('/') expects numbers.";
        return rtn;
      }
      rtn.numberValue /= child.numberValue;
      //printf("%lf\n", rtn.numberValue);
    }

  }

  return rtn;
}

Element ElementEvaluation::id(Element& e)
{
  std::map<std::string, Element>::iterator found;
  
  if (localVariables.size())
  {
    found = localVariables.back().find(e.stringValue);
    if (found != localVariables.back().end())
      return (found->second);
  }

  found = identifiers.find(e.stringValue);
  if (found == identifiers.end())
  {
    Element rtn = e;
    rtn.type = etError;
    rtn.stringValue = "Undefined identifier '" + rtn.stringValue + "'.";
    return rtn;
  }
  return (found->second);
}

Element ElementEvaluation::quote(Element& e)
{
  Element rtn = e;
  if (e.children.size() != 2)
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'quote' expects one argument.";
  }
  else
  {
    rtn = e.children[1];
  }
  return rtn;
}

Element ElementEvaluation::car(Element& e)
{
  Element rtn(e.parent);
  if (e.children.size() != 2)
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'car' expects one argument.";
    return rtn;
  }

  Element child1 = Evaluate(e.children[1]);

  if (child1.type != etList)
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'car' expects list.";
  }
  else if (child1.children.size() == 0)
  {
    rtn.type = etNil;
  }
  else
  {
    rtn = child1.children[0];
  }
  return rtn;
}

Element ElementEvaluation::cdr(Element& e)
{
  Element rtn(e.parent);
  if (e.children.size() != 2)
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'cdr' expects one argument.";
    return rtn;
  }

  Element child1 = Evaluate(e.children[1]);

  if (child1.type != etList)
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'cdr' expects list.";
  }
  else
  {
    rtn.type = etList;
    for (int i = 1; i < (int)child1.children.size(); i++)
      rtn.children.push_back(child1.children[i]);
  }
  return rtn;
}

Element ElementEvaluation::cmp(Element& e, cmptype op)
{
  if (e.children.size() <= 2)
  {
    Element rtn(e.parent);
    rtn.type = etError;
    rtn.stringValue = "Comparison expects at least 2 arguments.";
    return rtn;
  }
  Element child = Evaluate(e.children[1]);
  if (child.type != etNumber)
  {
    Element rtn(e.parent);
    rtn.type = etError;
    rtn.stringValue = "Comparison expects numbers.";
    return rtn;
  }

  Element rtn(e.parent);
  rtn.type = etTrue;
  lisp_number last = child.numberValue;

  for (int i = 2; i < (int)e.children.size(); i++)
  {
    child = Evaluate(e.children[i]);
    if (child.type != etNumber)
    {
      Element rtn(e.parent);
      rtn.type = etError;
      rtn.stringValue = "Comparison expects numbers.";
      return rtn;
    }
    bool ok;
    switch (op)
    {
    case cmpless:
      ok = (last < child.numberValue);
      break;
    case cmplequal:
      ok = (last <= child.numberValue);
      break;
    case cmpequal:
      ok = (last == child.numberValue);
      break;
    case cmpgequal:
      ok = (last >= child.numberValue);
      break;
    case cmpgreater:
      ok = (last > child.numberValue);
      break;
    case cmpnequal:
      ok = (last != child.numberValue);
      break;
    }

    if (ok)
      last = child.numberValue;
    else
    {
      rtn.type = etNil;
      return rtn;
    }
  }
  return rtn;
}

Element ElementEvaluation::cond(Element& e)
{
  Element rtn(e.parent);
  if (e.children.size() != 4 && e.children.size() != 3)
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'if' expects 2 or 3 arguments: (if (cond) (true-expr) [(false-expr)]).";
    return rtn;
  }

  Element child1 = Evaluate(e.children[1]);
  if (child1.type == etTrue)
  {
    rtn = Evaluate(e.children[2]);
  }
  else if (child1.type == etNil)
  {
    if (e.children.size() == 4)
      rtn = Evaluate(e.children[3]);
  }
  else
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'if' failed: Unexpected condition type.";
  }

  return rtn;
}

Element ElementEvaluation::defun(Element& e)
{
  Element rtn(e.parent);
  bool ok = true;
  if (e.children.size() != 4)
    ok = false;
  else if (e.children[1].type != etIdentifier || e.children[1].stringValue.empty())
    ok = false;
  else if (e.children[2].type != etList)
    ok = false;
  else
  {
    for (int i = 0; i < (int)e.children[2].children.size(); i++)
    {
      if (e.children[2].children[i].type != etIdentifier)
      {
	ok = false;
	break;
      }
    }
  }

  if (!ok) 
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'defun' expects these 3 arguments: (defun name (args) body).";
    return rtn;
  }
  
  functions[e.children[1].stringValue] = e;

  rtn.type = etTrue;
  return rtn;
}

Element ElementEvaluation::callcustom(Element& e)
{
  Element rtn(e.parent);
  Element& fn = functions[e.children[0].stringValue];

  int paramCount = fn.children[2].children.size();
  if ((int)e.children.size() - 1 != paramCount)
  {
    char num[128];
    sprintf(num, "%d", paramCount);
    rtn.type = etError;
    rtn.stringValue = "Function '" + e.children[0].stringValue + 
      "' expects " + num + " arguments.";
    return rtn;
  }

  /*
    pripraveni promennych
  */
  std::map<std::string, Element> variables;
  for (int i = 0; i < paramCount; i++)
  {
    Element value = Evaluate(e.children[i + 1]);
    if (value.type == etError)
      return value;
    //printf("%s\n", fn.children[2].children[i].stringValue.c_str());
    variables[fn.children[2].children[i].stringValue] = value;
  }

  /*
    ulozeni na zasobnik
  */
  localVariables.push_back(variables);
  /*
    volani funkce
  */
  rtn = Evaluate(fn.children[3]);
  /*
    zruseni zasobniku
  */
  localVariables.pop_back();
  return rtn;
}

Element ElementEvaluation::setid(Element& e)
{ 
  Element rtn(e.parent);
  bool ok = true;
  if (e.children.size() != 3)
    ok = false;
  else if (e.children[1].type != etIdentifier || e.children[1].stringValue.empty())
    ok = false;

  if (!ok) 
  {
    rtn.type = etError;
    rtn.stringValue = "Function 'setq' expects these 2 arguments: (setq id value).";
    return rtn;
  }
  
  rtn = Evaluate(e.children[2]);
  if (rtn.type != etError)
    identifiers[e.children[1].stringValue] = rtn;
  return rtn; 
}
