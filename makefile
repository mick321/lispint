CC=g++
CFLAGS=-c -Wall -O2
LDFLAGS=-O2
SOURCES=lexer.cpp lispint.cpp element.cpp eval.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=bin/lispint

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o
