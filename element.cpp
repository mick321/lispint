#include "element.h"
#include <stdio.h>

/*
  Vytiskni element.
*/
void Element::Print(bool linebreak)
{
  switch (type)
  {
    case etUnknown: printf("< ??? > "); break;
    case etNumber: printf(LISP_NUMBER_PRINT_FORMAT " ", numberValue); break;
    case etIdentifier: printf("%s ", stringValue.c_str()); break;
    case etList: 
      printf("( ");
      for (int i = 0; i < (int)children.size(); i++)
	children[i].Print(false);
      printf(")");
      break;
    case etError:
      printf("Error: %s", stringValue.c_str());
      break;
    case etNil:
      printf("< nil > ");
      break;
    case etTrue:
      printf("T ");
      break;
    default:;
  }
  if (linebreak)
    printf("\n");
}


/*
  Zacni budovat odznovu.
*/
void ElementBuilder::Reset()
{
  *e = Element(NULL);
  e->type = etList;
  level = e;
}

/*
  Pridej cislo do aktualni urovne.
*/
void ElementBuilder::AddNumber(const lisp_number num)
{
  Element newE(level);
  newE.type = etNumber;
  newE.numberValue = num;
  level->children.push_back(newE);
}

/*
  Pridej identifikator.
*/
void ElementBuilder::AddIdentifier(const std::string& identifier)
{
  Element newE(level);
  newE.type = etIdentifier;
  newE.stringValue = identifier;
  level->children.push_back(newE);
}

/*
  Pridej list.
*/
void ElementBuilder::AddList()
{
  Element newE(level);
  newE.type = etList;
  level->children.push_back(newE);
  level = &level->children.back();
}

/*
  Zakonci rozpracovany list.
*/
void ElementBuilder::EndList()
{
  level = level->parent;
}



