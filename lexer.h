#ifndef _LEXER_H
#define _LEXER_H

#define TOKEN_ERROR -1
#define TOKEN_UNKNOWN -2
#define TOKEN_EOF 0
#define TOKEN_IDENTIFIER 1
#define TOKEN_NUMBER 2
#define TOKEN_LBRACKET 3
#define TOKEN_RBRACKET 4
#define TOKEN_WHITESPACE 5
#define TOKEN_ENDLINE 6

#define MAX_TOKEN_LEN 128
#define MAX_ERR_LEN 1024

extern int     currentToken;
extern char    currentTokenStr[MAX_TOKEN_LEN];
extern double  currentTokenDouble;
extern char    currentTokenError[MAX_ERR_LEN];
extern char*   yytext;


extern int yylex (void);

#endif
